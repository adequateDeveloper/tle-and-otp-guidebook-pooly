defmodule Pooly.Server do
  @moduledoc """
  Communicates with both the top-level Supervisor and the WorkerSupervisor.
  It maintains a pool state that stores references to the top-level
  Supervisor and the WorkerSupervisor. It also stores details about the pool,
  such as the kind of worker processes to create and how many of them.

  Example pool configuration: [mfa: {SampleWorker, :start_link, []}, size: 5]

  When the top-level Supervisor starts this GenServer, it passes its own pid
  (sup) to the GenServer. Because this GenServer has a reference to the top-level
  Supervisor, it can tell the Supervisor to start a child process using the
  WorkerSupervisor. This GenServer passes the relevant bits of the pool
  configuration and WorkerSupervisor handles the rest.
  """

  use GenServer

  # TODO: deprecated. change to ?
  import Supervisor.Spec

  defmodule State do
    # TODO: rename these
    # sup: top-level supervisor pid
    # worker_sup: WorkerSupervisor pid
    # workers: list of worker pids
    # monitors: reference to the ets monitors table
    # mfa: module, function, arguments
    # size: pool size
    defstruct sup: nil, worker_sup: nil, workers: nil, mfa: nil, size: nil, monitors: nil
  end

  ## API

  # reference to the top-level Supervisor process (sup) and pool configuration
  def start_link(sup, pool_config) do
    GenServer.start_link(__MODULE__, [sup, pool_config], name: __MODULE__)
  end

  def checkout do
    GenServer.call(__MODULE__, :checkout)
  end

  def checkin(worker_pid) do
    GenServer.cast(__MODULE__, {:checkin, worker_pid})
  end

  def status do
    GenServer.call(__MODULE__, :status)
  end

  ## Callbacks

  # Validate options and initialize the state

  # validate top-level supervisor pid and store to state
  # Initialize the in-memory consumer <--> worker assignment table.
  def init([sup, pool_config]) when is_pid(sup) do
    monitors = :ets.new(:monitors, [:set, :private])
    init(pool_config, %State{sup: sup, monitors: monitors})
  end

  # Note: Each element in a keyword list can be represented by a two-element tuple,
  # where the first element is the key and the second element is the value.

  # pattern match on the mfa and store to state
  def init([{:mfa, mfa} | rest], state) do
    init(rest, %{state | mfa: mfa})
  end

  # pattern match on the size and store to state
  def init([{:size, size} | rest], state) do
    init(rest, %{state | size: size})
  end

  # ignore any remaining options
  def init([_ignore | rest], state) do
    init(rest, state)
  end

  # state is initialized, start WorkerSupervisor
  def init([], state) do
    # send/2 returns immediately so that init/2 isn't blocked
    send(self(), :start_worker_supervisor)
    {:ok, state}
  end

  # When a consumer process checks out a worker process from the pool, the following issues must be addressed:
  #   - what is the pid of the consumer process?
  #   - which worker process pid is the consumer using?
  #
  # The consumer process needs to be monitored by the server because if it dies,
  # the server process must know about it and take recovery action.
  # We also need to know which worker is assigned to which consumer process so that we can
  # pinpoint which consumer process used which worker pid.
  #
  # def handle_call(:checkout, _from, state) do
  # Note that _from is a two-element tuple consisting of the client pid and a tag (a reference).
  def handle_call(
        :checkout,
        {consumer_pid, _ref},
        %{workers: workers, monitors: monitors} = state
      ) do
    case workers do
      [worker | rest] ->
        # Server process will monitor the life of the consumer process
        consumer_ref = Process.monitor(consumer_pid)
        # store the monitored consumer_pid in the monitors table keyed by the assigned worker pid
        true = :ets.insert(monitors, {worker, consumer_ref})
        # update the state of the workers pool
        {:reply, worker, %{state | workers: rest}}

      [] ->
        # no processes available
        {:reply, :noproc, state}
    end
  end

  # the # of available workers and the # of checked out workers
  def handle_call(:status, _from, %{workers: workers, monitors: monitors} = state) do
    {:reply, {length(workers), :ets.info(monitors, :size)}, state}
  end

  # search for the worker pid in the ets monitors table.
  # if found, de-monitor the consumer process, remove the worker from the monitors table,
  # and update the workers pool with the checked in worker.
  def handle_cast({:checkin, worker}, %{workers: workers, monitors: monitors} = state) do
    case :ets.lookup(monitors, worker) do
      [{pid, ref}] ->
        true = Process.demonitor(ref)
        true = :ets.delete(monitors, pid)
        {:noreply, %{state | workers: [pid | workers]}}

      [] ->
        {:noreply, state}
    end
  end

  def handle_info(:start_worker_supervisor, state = %{sup: sup, mfa: mfa, size: size}) do
    # start the WorkerSupervisor with the top-level supervisor as its parent
    {:ok, worker_sup} = Supervisor.start_child(sup, supervisor_spec(mfa))

    workers = prepopulate(size, worker_sup)

    {:no_reply, %{state | worker_sup: worker_sup, workers: workers}}
  end

  # start the WorkerSupervisor as a supervisor process, not a worker process
  # :temporary means the top-level supervisor won't automatically restart the WorkerSupervisor.
  # The default automatic restart is turned off because of custom recovery rules.
  defp supervisor_spec(mfa) do
    opts = [restart: :temporary]
    supervisor(Pooly.WorkerSupervisor, [mfa], opts)
  end

  # prepopulate the WorkerSupervisor with a pool of worker processes
  defp prepopulate(size, sup) do
    prepopulate(size, sup, [])
  end

  defp prepopulate(size, _sup, workers) when size < 1 do
    workers
  end

  # create a list of worker processes attached to the supervisor
  defp prepopulate(size, sup, workers) do
    prepopulate(size - 1, sup, [new_worker(sup) | workers])
  end

  # dynamically create a worker process and attach to the supervisor
  defp new_worker(sup) do
    {:ok, worker_pid} = Supervisor.start_child(sup, [[]])
    worker_pid
  end
end
