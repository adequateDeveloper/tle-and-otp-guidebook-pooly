defmodule Pooly.WorkerSupervisor do
  @moduledoc """
  This Supervisor is responsible for monitoring all spawned workers in the pool.

  ## Examples

      $ iex -S mix
      iex> {:ok, worker_sup} = Pooly.WorkerSupervisor.start_link({SampleWorker, :start_link, []})
      iex> Supervisor.start_child(worker_sup, [[]])
      iex> Supervisor.which_children(worker_sup)
      iex> Supervisor.count_children(worker_sup)

      iex> {:ok, worker_pid} = Supervisor.start_child(worker_sup, [[]])
      iex> Supervisor.which_children(worker_sup)
      iex> SampleWorker.stop(worker_pid)
      iex>Supervisor.which_children(worker_sup)
  """

  use Supervisor

  ## API

  # Insure there is passed a tuple with module, fun, argument elements for the worker procss.
  def start_link({_, _, _} = mfa) do
    Supervisor.start_link(__MODULE__, mfa)
  end

  ## Callbacks

  def init({mod, fun, args}) do
    # the worker should always be restarted. fun is the function to start the worker (default: start_link())
    worker_opts = [restart: :permanent, function: fun]

    # the list of child process specifications
    children = [worker(mod, args, worker_opts)]

    # supervisor options. max # of restarts within max # of seconds before giving up
    sup_opts = [
      # TODO: change to Dynamic
      strategy: :simple_one_for_one,
      max_restarts: 5,
      max_seconds: 5
    ]

    # start the supervisor and create the child processes
    supervise(children, sup_opts)
  end
end
