defmodule SampleWorker do
  # to test Pooly.WorkerSupervisor. Note - not Pooly.SampleWorker

  use GenServer

  ## API

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, [])
  end

  def stop(pid) do
    GenServer.call(pid, :stop)
  end

  ## Callbacks

  def init(init_arg) do
    {:ok, init_arg}
  end

  def handle_call(:stop, _from, state) do
    {:stop, :normal, :ok, state}
  end
end
